﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FirebirdSql.Data.FirebirdClient;
using additional_classes;


namespace additional_functions
{
    public class additional_static_functions
    {
        /// <summary>
        /// method for denition of size changing
        /// </summary>
        /// <param name="form">user can't change the size of this form</param>
        public static void not_change_form_size(Form form)
        {
            form.FormBorderStyle = FormBorderStyle.FixedSingle;
            form.MaximizeBox = false;
        }

        /*method for loading datas from anything data tables*/
        public static void select_from_dt(FbConnection fbConnection,
            ref DataTable dt,
            DataGridView dataGrid, string sql_request) 
        {
            FbCommand fbCommand = new FbCommand(sql_request, fbConnection);

            fbConnection.Open();
            dt = new DataTable();
            dt.Load(fbCommand.ExecuteReader());
            dataGrid.DataSource = dt;
            fbConnection.Close();
        }

        /// <summary>
        /// method for loading datas from anything 
        ///data tables without adding on dataGridView
        /// </summary>
        /// <param name="fbConnection">fbconnection for connection with data base</param>
        /// <param name="dt">data table which will be used for writing in one some information</param>
        /// <param name="sql_request">load sql request for any table</param>
        public static void select_from_dt(FbConnection fbConnection,
            ref DataTable dt, string sql_request)  
        {
            FbCommand fbCommand = new FbCommand(sql_request, fbConnection);

            fbConnection.Open();
            dt = new DataTable();
            dt.Load(fbCommand.ExecuteReader());
            fbConnection.Close();
        }

        /// <summary>
        /// Method for inserting or deleting records from data table
        /// </summary>
        /// <param name="fbConnection">fbconnection for connection with data base</param>
        /// <param name="dt">data table where we will do changing</param>
        /// <param name="dataGrid">object for show data table</param>
        /// <param name="sql_request">sql request for executing</param>
        /// <param name="select_information_from_dt">sql request does 
        /// to show data table in datagridview </param>
        public static void insert_or_delete_in_dt(FbConnection fbConnection,
            ref DataTable dt,
            DataGridView dataGrid, string sql_request, string select_information_from_dt)
        {
            /*creating new object for executing sql requests*/
            FbCommand fbCommand = new FbCommand(sql_request, fbConnection); 
            
            fbConnection.Open();
            fbCommand.ExecuteNonQuery(); //execute request
            fbConnection.Close();

            //update table after inserting or deleting
            select_from_dt(fbConnection, ref dt, dataGrid, select_information_from_dt);             
        }

        /// <summary>
        /// Method for inserting or deleting records from data table
        /// without futher output on data grid view
        /// </summary>
        /// <param name="fbConnection">fbconnection for connection with data base</param>
        /// <param name="sql_request">sql request for executing</param>
        public static void insert_or_delete_in_dt(FbConnection fbConnection, string sql_request)
        {
            /*creating new object for executing sql requests*/
            FbCommand fbCommand = new FbCommand(sql_request, fbConnection);

            fbConnection.Open();
            fbCommand.ExecuteNonQuery(); //execute request
            fbConnection.Close();
        }

        public static void read_from_dt_to_comBox(ref ToolStripComboBox toolStripComboBox, 
            DataTable dataTable, int index_of_column)
        {
            for (int i = 0; i < dataTable.Rows.Count; i++)
            {
                toolStripComboBox.Items.Add(dataTable.Rows[i][index_of_column].ToString());
            }
        }

        public static void read_from_dt_to_comBox(ref ComboBox ComboBox, 
            DataTable dataTable, int index_of_column)
        {
            for (int i = 0; i < dataTable.Rows.Count; i++)
            {
                ComboBox.Items.Add(dataTable.Rows[i][index_of_column].ToString());
            }
        }

        public static void read_from_dt_to_comBox(ref ComboBox ComboBox,
            DataTableArr dataTable, int index_of_column)
        {
            for (int i = 0; i < dataTable.Rows.Count; i++)
            {
                ComboBox.Items.Add(dataTable[i,index_of_column].ToString());
            }
        }

        /// <summary>
        /// Autoincrementing id of records in data tables
        /// </summary>
        /// <param name="dataTable">data table where column was autoincremnet</param>
        /// <param name="index_of_id">number 0f column for autoincrementing</param>
        /// <returns></returns>
        public static int autoincrement_id(ref DataTable dataTable, int index_of_id)
        {
            int index_for_return;
            List<int> list_of_ids = new List<int>();
            for (int i = 0; i < dataTable.Rows.Count; i++) 
            {
                list_of_ids.Add((int)dataTable.Rows[i][index_of_id]);
            }
            list_of_ids.Sort();
            for(index_for_return = 1; index_for_return <= list_of_ids.Count; index_for_return++)
            {
                if (index_for_return != list_of_ids[index_for_return-1])
                    return index_for_return;
            }
            return index_for_return;
        }
        public static bool checking_on_other_table<J>(J verifiable_value, 
            DataTable dt_for_check, int index_of_value_in_table)
        {
            List<J> list_of_values = new List<J>();
            for(int i=0; i<dt_for_check.Rows.Count; i++)
            {
                list_of_values.Add((J)dt_for_check.Rows[i][index_of_value_in_table]);
            }
            return list_of_values.Contains(verifiable_value);
        }
        /// <summary>
        /// method for changing columns headers in data grid view
        /// </summary>
        /// <param name="dataGridView">data grid view for changing</param>
        /// <param name="array_of_columns_name">array with necessary names</param>
        public static void change_column_headers(DataGridView dataGridView, string[] array_of_columns_name)
        {
            for (int i = 0; i < array_of_columns_name.Length; i++)
               dataGridView.Columns[i].HeaderText = array_of_columns_name[i];
        }
        public static J[] read_title_id_to_combobox<J>(ref ComboBox comboBox, DataTable dataTable,
            int index_title_column, int index_id_column)
        {
            J[] arr_of_id = new J[dataTable.Rows.Count];
            read_from_dt_to_comBox(ref comboBox, dataTable, index_title_column);
            for(int i = 0; i < dataTable.Rows.Count; i++)
            {
                arr_of_id[i] = (J)dataTable.Rows[i][index_id_column];
            }
            return arr_of_id;
        }

        /// <summary>
        /// This function cheking the availability something in other tables. 
        /// 
        /// </summary>
        /// <param name="fbConn">fbconnection for connection with data base</param>
        /// <param name="request">request for searching in other data table</param>
        /// <param name="warn_text">text in message box, if it will be found</param>
        /// <param name="warn_title">title in message box, if it will be found</param>
        /// <returns>If checking position is in other tablea, then return true, else false.</returns>
        public static bool cheking_before_deleting(FbConnection fbConn, string request, 
            string warn_text, string warn_title)
        {
            DataTable datatb_for_check = null;
            select_from_dt(fbConn, ref datatb_for_check, request);
            if (datatb_for_check.Rows.Count != 0)
            {
                MessageBox.Show(warn_text, warn_title, MessageBoxButtons.OK, 
                    MessageBoxIcon.Warning);
                return true;
            }
            return false;
        }

        /// <summary>
        /// The method check is you need in choosen data table
        /// </summary>
        /// <param name="dataTable">choosen data table for checking</param>
        /// <param name="warn_text">text in message box, if it will be found</param>
        /// <param name="warn_title">title in message box, if it will be found</param>
        /// <returns>if checking position is in data table, then return true, else false </returns>
        public static bool cheking_before_deleting(ref DataTable dataTable, string warn_text, 
            string warn_title)
        {
            if (dataTable.Rows.Count == 0)
            {
                MessageBox.Show(warn_text, warn_title, MessageBoxButtons.OK,
                    MessageBoxIcon.Warning);
                return true;
            }
            return false;
        }
    }
}
