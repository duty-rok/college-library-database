﻿
namespace library_database_application
{
    partial class add_in_dt_pbhs
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.date_creating = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.publish_house_title = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // date_creating
            // 
            this.date_creating.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.date_creating.Location = new System.Drawing.Point(231, 72);
            this.date_creating.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.date_creating.Name = "date_creating";
            this.date_creating.Size = new System.Drawing.Size(226, 25);
            this.date_creating.TabIndex = 1;
            this.date_creating.Value = new System.DateTime(2021, 4, 5, 0, 0, 0, 0);
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(12, 70);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(166, 27);
            this.label1.TabIndex = 2;
            this.label1.Text = "Дата основания:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // publish_house_title
            // 
            this.publish_house_title.Location = new System.Drawing.Point(231, 9);
            this.publish_house_title.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.publish_house_title.Name = "publish_house_title";
            this.publish_house_title.Size = new System.Drawing.Size(226, 25);
            this.publish_house_title.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(12, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(166, 25);
            this.label2.TabIndex = 4;
            this.label2.Text = "Название издательсва:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("DejaVu Sans Condensed", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button1.Location = new System.Drawing.Point(22, 128);
            this.button1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(442, 41);
            this.button1.TabIndex = 5;
            this.button1.Text = "Добавить новое издательство";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // add_in_dt_pbhs
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(476, 186);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.publish_house_title);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.date_creating);
            this.Font = new System.Drawing.Font("DejaVu Sans Condensed", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "add_in_dt_pbhs";
            this.Text = "add_in_dt_pbhs";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DateTimePicker date_creating;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox publish_house_title;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button button1;
    }
}