﻿using System;
using System.Windows.Forms;
using static additional_functions.additional_static_functions;
using static library_database_application.Form1;
using System.Data;

namespace library_database_application
{
    public partial class data_table_publishing_houses : Form
    {
        public data_table_publishing_houses()
        {
            InitializeComponent();
            select_from_dt(fb_con, ref dt_publisihing_houses,dg_publishing_house, load_all_from_dt_pbhs);
            change_column_headers(dg_publishing_house, dg_pbhs_column_headers);
        }

        private void add_publishing_house_Click(object sender, EventArgs e)
        {
            add_in_dt_pbhs form_for_adding = new add_in_dt_pbhs(dg_publishing_house);
            form_for_adding.ShowDialog();
            form_for_adding.Close();
        }

        private void delete_from_dt_pbhs_Click(object sender, EventArgs e)
        {
            int id_del_publishing_houses, index_selected_row;
            if (dt_publisihing_houses.Rows.Count == 0)
            {
                MessageBox.Show("В этом списке пусто", "Удалять нечего",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }           
            index_selected_row = dg_publishing_house.SelectedRows[0].Index;
            id_del_publishing_houses = (int)dg_publishing_house[0, index_selected_row].Value;
            if (checking_on_other_table(id_del_publishing_houses,dt_books,1))
            {
                MessageBox.Show("У вас есть книги от этого издательсва", 
                    "Вы не можете удалить это издательство", 
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            insert_or_delete_in_dt(fb_con, ref dt_publisihing_houses, dg_publishing_house,
                $"delete from publishing_houses where ID_PUBLISHING_HOUSE = " +
                $"\'{dg_publishing_house[0,index_selected_row].Value}\';", load_all_from_dt_pbhs);
        }
    }
}
