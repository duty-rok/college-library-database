﻿using System;
using System.Windows.Forms;
using static additional_functions.additional_static_functions;
using static library_database_application.Form1;
using System.Data;

namespace library_database_application
{
    public partial class data_table_authors : Form
    {
        public data_table_authors()
        {
            InitializeComponent();
            select_from_dt(fb_con, ref dt_authors, dg_authors, load_all_from_dt_ath);
            change_column_headers(dg_authors, dg_ath_column_headers);
        }

        private void add_in_dt_ath_Click(object sender, EventArgs e)
        {
            add_in_dt_ath form_for_adding = new add_in_dt_ath(dg_authors);
            form_for_adding.ShowDialog();
            form_for_adding.Close();
        }

        //method for delete some author
        private void delete_from_dt_ath_Click(object sender, EventArgs e)
        {
            int id_del_author;
            if (dt_authors.Rows.Count == 0)
            {
                MessageBox.Show("В этом списке пусто", "Удалять некого",
                MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            id_del_author = (int)dg_authors[0, dg_authors.SelectedRows[0].Index].Value;
            //checking for using of some authors before deleting
            DataTable dt_for_check = null;
            select_from_dt(fb_con, ref dt_for_check, $"select * from " +
                $"AUTHORS_WITH_COMPOSITIONS where (id_author = {id_del_author})");
            if (dt_for_check.Rows.Count != 0)
            {
                MessageBox.Show("Есть произведения у которых указан данный автор",
                    "На этого автора ссылаются", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            //end of check

            insert_or_delete_in_dt(fb_con, ref dt_authors, dg_authors,
                $"delete from authors where id_author = \'{id_del_author}\';",
                load_all_from_dt_ath);
        }
    }
}
