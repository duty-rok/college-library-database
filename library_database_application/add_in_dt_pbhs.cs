﻿using System;
using System.Windows.Forms;
using static additional_functions.additional_static_functions;
using static library_database_application.Form1;

namespace library_database_application
{
    public partial class add_in_dt_pbhs : Form
    {
        DataGridView dataGridView;
        protected add_in_dt_pbhs()
        {
            InitializeComponent();
            date_creating.Value = DateTime.Now;
            not_change_form_size(this);
        }
        public add_in_dt_pbhs(DataGridView dataGridView) : this()
        {
            this.dataGridView = dataGridView;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (publish_house_title.Text.Length == 0)
            {
                MessageBox.Show("Вы не дали название для издательсва", "Не указаны нужные данные",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            if (publish_house_title.Text.Length > 60)
            {
                MessageBox.Show("Нужно указать название длиной менее 60", "Cлишком длинное название",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            insert_or_delete_in_dt(fb_con, ref dt_publisihing_houses, dataGridView, $"insert into " +
                $"PUBLISHING_HOUSES values(\'{autoincrement_id(ref dt_publisihing_houses, 0)}\'," +
                $"\'{publish_house_title.Text}\',\'{date_creating.Value.ToShortDateString()}\');",
                load_all_from_dt_pbhs);
            publish_house_title.Text = "";
        }
    }
}
