﻿
namespace library_database_application
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip2 = new System.Windows.Forms.MenuStrip();
            this.information_about_programm = new System.Windows.Forms.ToolStripMenuItem();
            this.добавитьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.удалитьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.library_users = new System.Windows.Forms.TabPage();
            this.dg_library_users = new System.Windows.Forms.DataGridView();
            this.menuStrip3 = new System.Windows.Forms.MenuStrip();
            this.add_in_dt_lu = new System.Windows.Forms.ToolStripMenuItem();
            this.show_on_groups = new System.Windows.Forms.ToolStripMenuItem();
            this.college_groups = new System.Windows.Forms.TabPage();
            this.dg_college_groups = new System.Windows.Forms.DataGridView();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.add_in_dt_cg = new System.Windows.Forms.ToolStripMenuItem();
            this.del_from_dt_cg = new System.Windows.Forms.ToolStripMenuItem();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.compositions = new System.Windows.Forms.TabPage();
            this.dg_compositions = new System.Windows.Forms.DataGridView();
            this.menuStrip5 = new System.Windows.Forms.MenuStrip();
            this.add_in_dt_cmpth = new System.Windows.Forms.ToolStripMenuItem();
            this.delete_from_dt_cmpth = new System.Windows.Forms.ToolStripMenuItem();
            this.add_some_autors = new System.Windows.Forms.ToolStripMenuItem();
            this.add_authors_for_сmth = new System.Windows.Forms.ToolStripMenuItem();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.dg_books = new System.Windows.Forms.DataGridView();
            this.menuStrip7 = new System.Windows.Forms.MenuStrip();
            this.add_in_dt_books = new System.Windows.Forms.ToolStripMenuItem();
            this.delete_from_dt_books = new System.Windows.Forms.ToolStripMenuItem();
            this.work_with_pub_house = new System.Windows.Forms.ToolStripMenuItem();
            this.pbhs_with_book_relation = new System.Windows.Forms.ToolStripMenuItem();
            this.extraditions_books = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip2.SuspendLayout();
            this.library_users.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dg_library_users)).BeginInit();
            this.menuStrip3.SuspendLayout();
            this.college_groups.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dg_college_groups)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.compositions.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dg_compositions)).BeginInit();
            this.menuStrip5.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dg_books)).BeginInit();
            this.menuStrip7.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip2
            // 
            this.menuStrip2.Font = new System.Drawing.Font("Victor Mono Oblique", 9.749999F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.menuStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.information_about_programm});
            this.menuStrip2.Location = new System.Drawing.Point(0, 0);
            this.menuStrip2.Name = "menuStrip2";
            this.menuStrip2.Padding = new System.Windows.Forms.Padding(7, 2, 0, 2);
            this.menuStrip2.Size = new System.Drawing.Size(800, 24);
            this.menuStrip2.TabIndex = 1;
            this.menuStrip2.Text = "menuStrip2";
            // 
            // information_about_programm
            // 
            this.information_about_programm.Name = "information_about_programm";
            this.information_about_programm.Size = new System.Drawing.Size(97, 20);
            this.information_about_programm.Text = "О программе";
            this.information_about_programm.Click += new System.EventHandler(this.information_about_programm_Click);
            // 
            // добавитьToolStripMenuItem
            // 
            this.добавитьToolStripMenuItem.Name = "добавитьToolStripMenuItem";
            this.добавитьToolStripMenuItem.Size = new System.Drawing.Size(80, 20);
            this.добавитьToolStripMenuItem.Text = "Добавить...";
            this.добавитьToolStripMenuItem.Click += new System.EventHandler(this.add_in_dt_cg_Click);
            // 
            // удалитьToolStripMenuItem
            // 
            this.удалитьToolStripMenuItem.Name = "удалитьToolStripMenuItem";
            this.удалитьToolStripMenuItem.Size = new System.Drawing.Size(72, 20);
            this.удалитьToolStripMenuItem.Text = "Удалить...";
            this.удалитьToolStripMenuItem.Click += new System.EventHandler(this.del_from_dt_cg_Click);
            // 
            // library_users
            // 
            this.library_users.Controls.Add(this.dg_library_users);
            this.library_users.Controls.Add(this.menuStrip3);
            this.library_users.Location = new System.Drawing.Point(4, 24);
            this.library_users.Name = "library_users";
            this.library_users.Padding = new System.Windows.Forms.Padding(3);
            this.library_users.Size = new System.Drawing.Size(792, 394);
            this.library_users.TabIndex = 1;
            this.library_users.Text = "Пользователи Библотеки";
            this.library_users.UseVisualStyleBackColor = true;
            // 
            // dg_library_users
            // 
            this.dg_library_users.AllowUserToAddRows = false;
            this.dg_library_users.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dg_library_users.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dg_library_users.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dg_library_users.Location = new System.Drawing.Point(3, 27);
            this.dg_library_users.Name = "dg_library_users";
            this.dg_library_users.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dg_library_users.Size = new System.Drawing.Size(786, 364);
            this.dg_library_users.TabIndex = 1;
            // 
            // menuStrip3
            // 
            this.menuStrip3.Font = new System.Drawing.Font("DejaVu Sans Condensed", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.menuStrip3.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.add_in_dt_lu,
            this.show_on_groups,
            this.extraditions_books});
            this.menuStrip3.Location = new System.Drawing.Point(3, 3);
            this.menuStrip3.Name = "menuStrip3";
            this.menuStrip3.Padding = new System.Windows.Forms.Padding(7, 2, 0, 2);
            this.menuStrip3.Size = new System.Drawing.Size(786, 24);
            this.menuStrip3.TabIndex = 0;
            this.menuStrip3.Text = "menuStrip3";
            // 
            // add_in_dt_lu
            // 
            this.add_in_dt_lu.Name = "add_in_dt_lu";
            this.add_in_dt_lu.Size = new System.Drawing.Size(82, 20);
            this.add_in_dt_lu.Text = "Добавить...";
            this.add_in_dt_lu.Click += new System.EventHandler(this.add_in_dt_lu_Click);
            // 
            // show_on_groups
            // 
            this.show_on_groups.Name = "show_on_groups";
            this.show_on_groups.Size = new System.Drawing.Size(245, 20);
            this.show_on_groups.Text = "Показать по группам или выдать книгу";
            this.show_on_groups.Click += new System.EventHandler(this.show_on_groups_Click);
            // 
            // college_groups
            // 
            this.college_groups.Controls.Add(this.dg_college_groups);
            this.college_groups.Controls.Add(this.menuStrip1);
            this.college_groups.Location = new System.Drawing.Point(4, 24);
            this.college_groups.Name = "college_groups";
            this.college_groups.Padding = new System.Windows.Forms.Padding(3);
            this.college_groups.Size = new System.Drawing.Size(792, 394);
            this.college_groups.TabIndex = 0;
            this.college_groups.Text = "Группы техникума";
            this.college_groups.UseVisualStyleBackColor = true;
            // 
            // dg_college_groups
            // 
            this.dg_college_groups.AllowUserToAddRows = false;
            this.dg_college_groups.AllowUserToDeleteRows = false;
            this.dg_college_groups.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dg_college_groups.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dg_college_groups.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dg_college_groups.Location = new System.Drawing.Point(3, 27);
            this.dg_college_groups.MultiSelect = false;
            this.dg_college_groups.Name = "dg_college_groups";
            this.dg_college_groups.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dg_college_groups.Size = new System.Drawing.Size(786, 364);
            this.dg_college_groups.TabIndex = 0;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Font = new System.Drawing.Font("DejaVu Sans Condensed", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.add_in_dt_cg,
            this.del_from_dt_cg});
            this.menuStrip1.Location = new System.Drawing.Point(3, 3);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(7, 2, 0, 2);
            this.menuStrip1.Size = new System.Drawing.Size(786, 24);
            this.menuStrip1.TabIndex = 1;
            // 
            // add_in_dt_cg
            // 
            this.add_in_dt_cg.Name = "add_in_dt_cg";
            this.add_in_dt_cg.Size = new System.Drawing.Size(82, 20);
            this.add_in_dt_cg.Text = "Добавить...";
            this.add_in_dt_cg.Click += new System.EventHandler(this.add_in_dt_cg_Click);
            // 
            // del_from_dt_cg
            // 
            this.del_from_dt_cg.Name = "del_from_dt_cg";
            this.del_from_dt_cg.Size = new System.Drawing.Size(66, 20);
            this.del_from_dt_cg.Text = "Удалить";
            this.del_from_dt_cg.Click += new System.EventHandler(this.del_from_dt_cg_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.college_groups);
            this.tabControl1.Controls.Add(this.library_users);
            this.tabControl1.Controls.Add(this.compositions);
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Font = new System.Drawing.Font("DejaVu Sans", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tabControl1.Location = new System.Drawing.Point(0, 24);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(800, 422);
            this.tabControl1.TabIndex = 0;
            // 
            // compositions
            // 
            this.compositions.Controls.Add(this.dg_compositions);
            this.compositions.Controls.Add(this.menuStrip5);
            this.compositions.Location = new System.Drawing.Point(4, 24);
            this.compositions.Name = "compositions";
            this.compositions.Padding = new System.Windows.Forms.Padding(3);
            this.compositions.Size = new System.Drawing.Size(792, 394);
            this.compositions.TabIndex = 3;
            this.compositions.Text = "Произведения";
            this.compositions.UseVisualStyleBackColor = true;
            // 
            // dg_compositions
            // 
            this.dg_compositions.AllowUserToAddRows = false;
            this.dg_compositions.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dg_compositions.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dg_compositions.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dg_compositions.Location = new System.Drawing.Point(3, 27);
            this.dg_compositions.MultiSelect = false;
            this.dg_compositions.Name = "dg_compositions";
            this.dg_compositions.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dg_compositions.Size = new System.Drawing.Size(786, 364);
            this.dg_compositions.TabIndex = 1;
            // 
            // menuStrip5
            // 
            this.menuStrip5.Font = new System.Drawing.Font("DejaVu Sans Condensed", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.menuStrip5.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.add_in_dt_cmpth,
            this.delete_from_dt_cmpth,
            this.add_some_autors,
            this.add_authors_for_сmth});
            this.menuStrip5.Location = new System.Drawing.Point(3, 3);
            this.menuStrip5.Name = "menuStrip5";
            this.menuStrip5.Size = new System.Drawing.Size(786, 24);
            this.menuStrip5.TabIndex = 0;
            this.menuStrip5.Text = "menuStrip5";
            // 
            // add_in_dt_cmpth
            // 
            this.add_in_dt_cmpth.Name = "add_in_dt_cmpth";
            this.add_in_dt_cmpth.Size = new System.Drawing.Size(82, 20);
            this.add_in_dt_cmpth.Text = "Добавить...";
            this.add_in_dt_cmpth.Click += new System.EventHandler(this.add_in_dt_cmpth_Click);
            // 
            // delete_from_dt_cmpth
            // 
            this.delete_from_dt_cmpth.Name = "delete_from_dt_cmpth";
            this.delete_from_dt_cmpth.Size = new System.Drawing.Size(66, 20);
            this.delete_from_dt_cmpth.Text = "Удалить";
            this.delete_from_dt_cmpth.Click += new System.EventHandler(this.delete_from_dt_cmpth_Click);
            // 
            // add_some_autors
            // 
            this.add_some_autors.Name = "add_some_autors";
            this.add_some_autors.Size = new System.Drawing.Size(117, 20);
            this.add_some_autors.Text = "Таблица Авторов";
            this.add_some_autors.Click += new System.EventHandler(this.add_some_autors_Click);
            // 
            // add_authors_for_сmth
            // 
            this.add_authors_for_сmth.Name = "add_authors_for_сmth";
            this.add_authors_for_сmth.Size = new System.Drawing.Size(200, 20);
            this.add_authors_for_сmth.Text = "Показать авторов произвдения";
            this.add_authors_for_сmth.Click += new System.EventHandler(this.add_authors_for_cmth_Click);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.dg_books);
            this.tabPage1.Controls.Add(this.menuStrip7);
            this.tabPage1.Location = new System.Drawing.Point(4, 24);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(792, 394);
            this.tabPage1.TabIndex = 5;
            this.tabPage1.Text = "Книги";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // dg_books
            // 
            this.dg_books.AllowUserToAddRows = false;
            this.dg_books.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dg_books.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dg_books.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dg_books.Location = new System.Drawing.Point(3, 27);
            this.dg_books.MultiSelect = false;
            this.dg_books.Name = "dg_books";
            this.dg_books.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dg_books.Size = new System.Drawing.Size(786, 364);
            this.dg_books.TabIndex = 3;
            // 
            // menuStrip7
            // 
            this.menuStrip7.Font = new System.Drawing.Font("DejaVu Sans Condensed", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.menuStrip7.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.add_in_dt_books,
            this.delete_from_dt_books,
            this.work_with_pub_house,
            this.pbhs_with_book_relation});
            this.menuStrip7.Location = new System.Drawing.Point(3, 3);
            this.menuStrip7.Name = "menuStrip7";
            this.menuStrip7.Size = new System.Drawing.Size(786, 24);
            this.menuStrip7.TabIndex = 0;
            this.menuStrip7.Text = "menuStrip7";
            // 
            // add_in_dt_books
            // 
            this.add_in_dt_books.Name = "add_in_dt_books";
            this.add_in_dt_books.Size = new System.Drawing.Size(82, 20);
            this.add_in_dt_books.Text = "Добавить...";
            this.add_in_dt_books.Click += new System.EventHandler(this.add_in_dt_books_Click);
            // 
            // delete_from_dt_books
            // 
            this.delete_from_dt_books.Name = "delete_from_dt_books";
            this.delete_from_dt_books.Size = new System.Drawing.Size(102, 20);
            this.delete_from_dt_books.Text = "Удалить книгу";
            this.delete_from_dt_books.Click += new System.EventHandler(this.delete_from_dt_books_Click);
            // 
            // work_with_pub_house
            // 
            this.work_with_pub_house.Name = "work_with_pub_house";
            this.work_with_pub_house.Size = new System.Drawing.Size(142, 20);
            this.work_with_pub_house.Text = "Таблица издательств";
            this.work_with_pub_house.Click += new System.EventHandler(this.work_with_pub_house_Click);
            // 
            // pbhs_with_book_relation
            // 
            this.pbhs_with_book_relation.Name = "pbhs_with_book_relation";
            this.pbhs_with_book_relation.Size = new System.Drawing.Size(203, 20);
            this.pbhs_with_book_relation.Text = "Добавить произведение в книгу";
            this.pbhs_with_book_relation.Click += new System.EventHandler(this.pbhs_with_book_relation_Click);
            // 
            // extraditions_books
            // 
            this.extraditions_books.Name = "extraditions_books";
            this.extraditions_books.Size = new System.Drawing.Size(12, 20);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 446);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.menuStrip2);
            this.Font = new System.Drawing.Font("DejaVu Sans", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "База данных по учету книг в библиотеке ";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.menuStrip2.ResumeLayout(false);
            this.menuStrip2.PerformLayout();
            this.library_users.ResumeLayout(false);
            this.library_users.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dg_library_users)).EndInit();
            this.menuStrip3.ResumeLayout(false);
            this.menuStrip3.PerformLayout();
            this.college_groups.ResumeLayout(false);
            this.college_groups.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dg_college_groups)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.compositions.ResumeLayout(false);
            this.compositions.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dg_compositions)).EndInit();
            this.menuStrip5.ResumeLayout(false);
            this.menuStrip5.PerformLayout();
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dg_books)).EndInit();
            this.menuStrip7.ResumeLayout(false);
            this.menuStrip7.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.MenuStrip menuStrip2;
        private System.Windows.Forms.ToolStripMenuItem добавитьToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem удалитьToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem information_about_programm;
        private System.Windows.Forms.TabPage library_users;
        private System.Windows.Forms.TabPage college_groups;
        private System.Windows.Forms.DataGridView dg_college_groups;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem add_in_dt_cg;
        private System.Windows.Forms.ToolStripMenuItem del_from_dt_cg;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.DataGridView dg_library_users;
        private System.Windows.Forms.MenuStrip menuStrip3;
        private System.Windows.Forms.ToolStripMenuItem add_in_dt_lu;
        private System.Windows.Forms.ToolStripMenuItem show_on_groups;
        private System.Windows.Forms.TabPage compositions;
        private System.Windows.Forms.DataGridView dg_compositions;
        private System.Windows.Forms.MenuStrip menuStrip5;
        private System.Windows.Forms.ToolStripMenuItem add_in_dt_cmpth;
        private System.Windows.Forms.ToolStripMenuItem delete_from_dt_cmpth;
        private System.Windows.Forms.ToolStripMenuItem add_authors_for_сmth;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.DataGridView dg_books;
        private System.Windows.Forms.MenuStrip menuStrip7;
        private System.Windows.Forms.ToolStripMenuItem add_in_dt_books;
        private System.Windows.Forms.ToolStripMenuItem delete_from_dt_books;
        private System.Windows.Forms.ToolStripMenuItem add_some_autors;
        private System.Windows.Forms.ToolStripMenuItem work_with_pub_house;
        private System.Windows.Forms.ToolStripMenuItem pbhs_with_book_relation;
        private System.Windows.Forms.ToolStripMenuItem extraditions_books;
    }
}

