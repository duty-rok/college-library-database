﻿using System;
using System.Windows.Forms;
using static additional_functions.additional_static_functions;
using static library_database_application.Form1;
using System.Data;

namespace library_database_application
{
    public partial class relation_book_with_compostions : Form
    {
        int id_book;

        DataTable dt_available_cmps, dt_exists_cmps;
        string request_av_cmps, request_ex_cmps;
        string[] column_headers = { "", "Название произведения", "Дата публикации" };

        private void relation_book_with_compostions_Load(object sender, EventArgs e)
        {
            method_for_updation();
            dg_available_compositions.Columns[0].Visible = false;
            dg_exists_composition.Columns[0].Visible = false;
            change_column_headers(dg_available_compositions, column_headers);
            change_column_headers(dg_exists_composition, column_headers);
        }

        protected relation_book_with_compostions()
        {
            InitializeComponent();
            not_change_form_size(this);
        }

        private void link_cmps_with_book_Click(object sender, EventArgs e)
        {
            if (dt_available_cmps.Rows.Count == 0)
            {
                MessageBox.Show("Для того чтобы добавить произведение к данной книге\n" +
                    "Добавьте новое в таблицу произведений",
                    "У вас нет доступных произведений для добавления",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            insert_or_delete_in_dt(fb_con, $"insert into BOOKS_WITH_COMPOSITIONS values(" +
                $"\'{dg_available_compositions[0, dg_available_compositions.SelectedRows[0].Index].Value}\'," +
                $"\'{id_book}\');");
            method_for_updation();
        }

        private void delete_cmps_Click(object sender, EventArgs e)
        {
            if (dt_exists_cmps.Rows.Count == 0)
            {
                MessageBox.Show("В этой книге нет произведений, сначала добавьте их",
                    "Нет произведений для удаления",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            insert_or_delete_in_dt(fb_con, $"delete from BOOKS_WITH_COMPOSITIONS bwc where (" +
                $"(bwc.id_book = {id_book}) and " +
                $"(bwc.id_composition =" +
                $" {dg_exists_composition[0, dg_exists_composition.SelectedRows[0].Index].Value}));");
            method_for_updation();
        }

        public relation_book_with_compostions(int id_book, string title) : this()
        {
            this.id_book = id_book;
            this.title.Text = title;
            request_av_cmps = $"select c.* from compositions c where not exists(" +
                $"select * from books_with_compositions bwc where(" +
                $"(bwc.id_book = {id_book}) and (bwc.id_composition = c.id_composition)));";
            request_ex_cmps = $"select c.* from compositions c where exists(" +
                $"select * from books_with_compositions bwc where(" +
                $"(bwc.id_book = {id_book}) and (bwc.id_composition = c.id_composition)));";
        }
        void method_for_updation()
        {
            select_from_dt(fb_con, ref dt_available_cmps, dg_available_compositions, request_av_cmps);
            select_from_dt(fb_con, ref dt_exists_cmps, dg_exists_composition,request_ex_cmps);
        }
    }
}
