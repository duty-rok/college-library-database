﻿
namespace library_database_application
{
    partial class form_show_on_groups
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.group_container = new System.Windows.Forms.ToolStripComboBox();
            this.delete_from_lu = new System.Windows.Forms.ToolStripMenuItem();
            this.extradition_of_books = new System.Windows.Forms.ToolStripMenuItem();
            this.dg_sortiration = new System.Windows.Forms.DataGridView();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dg_sortiration)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Font = new System.Drawing.Font("DejaVu Sans", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.group_container,
            this.delete_from_lu,
            this.extradition_of_books});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(687, 27);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // group_container
            // 
            this.group_container.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.group_container.Name = "group_container";
            this.group_container.Size = new System.Drawing.Size(121, 23);
            this.group_container.SelectedIndexChanged += new System.EventHandler(this.group_is_chosen);
            // 
            // delete_from_lu
            // 
            this.delete_from_lu.Name = "delete_from_lu";
            this.delete_from_lu.Size = new System.Drawing.Size(169, 23);
            this.delete_from_lu.Text = "Удалить пользователя";
            this.delete_from_lu.Click += new System.EventHandler(this.delete_from_lu_Click);
            // 
            // extradition_of_books
            // 
            this.extradition_of_books.Name = "extradition_of_books";
            this.extradition_of_books.Size = new System.Drawing.Size(102, 23);
            this.extradition_of_books.Text = "Выдача кинг";
            this.extradition_of_books.Click += new System.EventHandler(this.extradition_of_books_Click);
            // 
            // dg_sortiration
            // 
            this.dg_sortiration.AllowUserToAddRows = false;
            this.dg_sortiration.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dg_sortiration.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dg_sortiration.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dg_sortiration.Location = new System.Drawing.Point(0, 27);
            this.dg_sortiration.MultiSelect = false;
            this.dg_sortiration.Name = "dg_sortiration";
            this.dg_sortiration.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dg_sortiration.Size = new System.Drawing.Size(687, 279);
            this.dg_sortiration.TabIndex = 1;
            // 
            // form_show_on_groups
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(687, 306);
            this.Controls.Add(this.dg_sortiration);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "form_show_on_groups";
            this.Text = "сортировка пользователей по группам";
            this.Load += new System.EventHandler(this.form_show_on_groups_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dg_sortiration)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripComboBox group_container;
        private System.Windows.Forms.DataGridView dg_sortiration;
        private System.Windows.Forms.ToolStripMenuItem delete_from_lu;
        private System.Windows.Forms.ToolStripMenuItem extradition_of_books;
    }
}