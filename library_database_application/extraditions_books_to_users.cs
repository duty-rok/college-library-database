﻿using System;
using System.Windows.Forms;
using static additional_functions.additional_static_functions;
using static library_database_application.Form1;
using System.Data;
using System.Collections.Generic;
using System.Linq;


namespace library_database_application
{
    public partial class extraditions_books_to_users : Form
    {
        DataTable dt_given_books, dt_books_for_extradition;
        string load_given_books, load_books_for_extradition;
        int id_user;
        string[] dg_books_for_extradition_column_headers = { "", "Название книги", "Библиотечный номер", "Издательсво","Кол-во страниц" },
            dg_givenb_column_headers = { "", "Название книги", "Библиотечный номер", "Издательсво", "Дата выдачи" };

        private void extraditions_books_to_users_Load(object sender, EventArgs e)
        {
            method_for_updation();
            change_column_headers(dg_books_for_extradition,
                dg_books_for_extradition_column_headers);
            change_column_headers(dg_given_books, dg_givenb_column_headers);

            dg_books_for_extradition.Columns[0].Visible = false;
            dg_given_books.Columns[0].Visible = false;
        }

        //give book to user
        private void issue_book_Click(object sender, EventArgs e)
        {
            if (dg_books_for_extradition.SelectedRows.Count == 0)
            {
                MessageBox.Show("Нет доступных книг для выдачи","В библиотеке закончились книги",
                    MessageBoxButtons.OK,MessageBoxIcon.Warning);
                return;
            }
            int row_index = dg_books_for_extradition.SelectedRows[0].Index;
            insert_or_delete_in_dt(fb_con, $"insert into actions_under_books values(" +
                $"\'{dg_books_for_extradition[0, row_index].Value}\'," +
                $"\'{id_user}\',\'{DateTime.Now.ToShortDateString()}\')");
            method_for_updation();
            updation?.Invoke();
        }

        //user return book
        private void return_book_Click(object sender, EventArgs e)
        {
            if (dg_given_books.SelectedRows.Count == 0)
            {
                MessageBox.Show("Нет доступных книг для возврата", 
                    "Пользователь библиотеки вам ничего недолжен",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            int row_index = dg_given_books.SelectedRows[0].Index;
            insert_or_delete_in_dt(fb_con, $"delete from actions_under_books where (" +
                $"(id_book = {dg_given_books[0, row_index].Value}) and (id_user = \'{id_user}\'));");
            method_for_updation();
            updation?.Invoke();
        }

        private void author_name_TextChanged(object sender, EventArgs e)
        {
            update_sort();
        }

        //system of searching for user
        void update_sort()
        {
            DataTable dt = null;
            IEnumerable<DataRow> res;
            if (author_name.Text == "")
            {
                dt = dt_books_for_extradition;
            }
            else
            {
                string request = $"select b.id_book, b.book_title, b.library_number, pbhs.title, b.page_amount " +
                    $"from books b, publishing_houses pbhs where(" +
                    $"(not exists (select aub.* from ACTIONS_UNDER_BOOKS aub " +
                    $"where((aub.id_book = b.id_book) and (aub.id_user = {id_user})))) " +
                    $"and (b.id_publishing_house = pbhs.id_publishing_house) and exists " +
                    $"(select * from books_with_compositions bwc where(" +
                    $"(exists (select * from authors_with_compositions awc where(" +
                    $"(exists (select * from authors a where(" +
                    $"(UPPER(a.surname) starting with \'{author_name.Text.ToUpper()}\') and (awc.id_author = a.id_author)) )) " +
                    $"and (bwc.id_composition = awc.id_composition)))) " +
                    $"and (bwc.id_book = b.id_book))));";
                select_from_dt(fb_con, ref dt, request);
            }
            res = dt.AsEnumerable();
            if (pages_amount.Text.Length != 0)
            {
                if (!char.IsDigit(pages_amount.Text[pages_amount.Text.Length - 1]))
                {
                    MessageBox.Show("Вы ввели не цифру");
                    pages_amount.Text = pages_amount.Text.Substring(0,pages_amount.Text.Length-1);
                }
                res = from t in res where t.Field<int>(4) <= Convert.ToInt32(pages_amount.Text) select t;
            }
            res = from t in res
                  where t.Field<string>(3).ToUpper().StartsWith(
                    pbhs_name.Text.ToUpper()) & t.Field<string>(1).ToUpper().StartsWith(
                    book_title.Text.ToUpper())
                    select t;
            if (res.Count() != 0)
            {
                dg_books_for_extradition.DataSource = res.CopyToDataTable();
                change_column_headers(dg_books_for_extradition, 
                    dg_books_for_extradition_column_headers);
                dg_books_for_extradition.Columns[0].Visible = false;
            }
            else
            {
                dg_books_for_extradition.DataSource = null;
            }
        }

        private void pages_amount_TextChanged(object sender, EventArgs e)
        {
            update_sort();
        }

        private void pbhs_name_TextChanged(object sender, EventArgs e)
        {
            update_sort();
        }

        //active of disactive searching system
        private void search_available_CheckedChanged(object sender, EventArgs e)
        {
            if (search_available.Checked)
            {
                panel1.Enabled = true;
                return;
            }
            panel1.Enabled = false;
            method_for_updation();
            pages_amount.Text = "";
            pbhs_name.Text = "";
            author_name.Text = "";
            book_title.Text = "";
        }

        private void book_title_TextChanged(object sender, EventArgs e)
        {
            update_sort();
        }

        protected extraditions_books_to_users()
        {
            InitializeComponent();
            not_change_form_size(this);
        }

        public extraditions_books_to_users
            (int id_user, string surname, string name, string group) : this()
        {
            this.surname.Text = surname;
            this.name.Text = name;
            this.group.Text = group;
            this.id_user = id_user;

            load_books_for_extradition = $"select b.id_book, b.book_title, b.library_number, pbhs.title, b.page_amount " +
                $"from books b, publishing_houses pbhs where(" +
                $"(not exists (select aub.* from ACTIONS_UNDER_BOOKS aub " +
                $"where((aub.id_book = b.id_book) and (aub.id_user = {id_user})))) " +
                $"and (b.id_publishing_house = pbhs.id_publishing_house));";

            load_given_books = $"select b.id_book, b.book_title, b.library_number, pbhs.title, aubo.EXTRADITION_DATE " +
                $"from books b, publishing_houses pbhs, ACTIONS_UNDER_BOOKS aubo where(" +
                $"(exists (select aub.* from ACTIONS_UNDER_BOOKS aub " +
                $"where((aub.id_book = b.id_book) and (aub.id_user = {id_user})))) " +
                $"and (b.id_publishing_house = pbhs.id_publishing_house) " +
                $"and (aubo.id_book = b.id_book));";            
        }
        void method_for_updation()
        {
            select_from_dt(fb_con, ref dt_books_for_extradition, dg_books_for_extradition, load_books_for_extradition);
            select_from_dt(fb_con, ref dt_given_books, dg_given_books, load_given_books);
        }
    }
}
