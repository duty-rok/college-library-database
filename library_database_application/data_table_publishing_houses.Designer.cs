﻿
namespace library_database_application
{
    partial class data_table_publishing_houses
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dg_publishing_house = new System.Windows.Forms.DataGridView();
            this.menuStrip6 = new System.Windows.Forms.MenuStrip();
            this.add_publishing_house = new System.Windows.Forms.ToolStripMenuItem();
            this.delete_from_dt_pbhs = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.dg_publishing_house)).BeginInit();
            this.menuStrip6.SuspendLayout();
            this.SuspendLayout();
            // 
            // dg_publishing_house
            // 
            this.dg_publishing_house.AllowUserToAddRows = false;
            this.dg_publishing_house.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dg_publishing_house.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dg_publishing_house.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dg_publishing_house.Location = new System.Drawing.Point(0, 24);
            this.dg_publishing_house.MultiSelect = false;
            this.dg_publishing_house.Name = "dg_publishing_house";
            this.dg_publishing_house.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dg_publishing_house.Size = new System.Drawing.Size(743, 365);
            this.dg_publishing_house.TabIndex = 4;
            // 
            // menuStrip6
            // 
            this.menuStrip6.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.add_publishing_house,
            this.delete_from_dt_pbhs});
            this.menuStrip6.Location = new System.Drawing.Point(0, 0);
            this.menuStrip6.Name = "menuStrip6";
            this.menuStrip6.Size = new System.Drawing.Size(743, 24);
            this.menuStrip6.TabIndex = 3;
            this.menuStrip6.Text = "menuStrip6";
            // 
            // add_publishing_house
            // 
            this.add_publishing_house.Name = "add_publishing_house";
            this.add_publishing_house.Size = new System.Drawing.Size(191, 20);
            this.add_publishing_house.Text = "Добавить новое издательство...";
            this.add_publishing_house.Click += new System.EventHandler(this.add_publishing_house_Click);
            // 
            // delete_from_dt_pbhs
            // 
            this.delete_from_dt_pbhs.Name = "delete_from_dt_pbhs";
            this.delete_from_dt_pbhs.Size = new System.Drawing.Size(133, 20);
            this.delete_from_dt_pbhs.Text = "Удалить издательсво";
            this.delete_from_dt_pbhs.Click += new System.EventHandler(this.delete_from_dt_pbhs_Click);
            // 
            // data_table_publishing_houses
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(743, 389);
            this.Controls.Add(this.dg_publishing_house);
            this.Controls.Add(this.menuStrip6);
            this.Font = new System.Drawing.Font("DejaVu Sans Condensed", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Name = "data_table_publishing_houses";
            this.Text = "Таблица издательств";
            ((System.ComponentModel.ISupportInitialize)(this.dg_publishing_house)).EndInit();
            this.menuStrip6.ResumeLayout(false);
            this.menuStrip6.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dg_publishing_house;
        private System.Windows.Forms.MenuStrip menuStrip6;
        private System.Windows.Forms.ToolStripMenuItem add_publishing_house;
        private System.Windows.Forms.ToolStripMenuItem delete_from_dt_pbhs;
    }
}