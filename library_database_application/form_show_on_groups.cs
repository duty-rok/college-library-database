﻿using System;
using System.Windows.Forms;
using static additional_functions.additional_static_functions;
using System.Data;
using static library_database_application.Form1;

namespace library_database_application
{
    public partial class form_show_on_groups : Form
    {
        //data table for sorted data
        DataTable dt_sorted_list = null;
        DataGridView dg_library_users;

        public form_show_on_groups()
        {
            InitializeComponent();
            not_change_form_size(this);            
        }

        private void group_is_chosen(object sender, EventArgs e)
        {
            //execute saved procedure form database
            select_from_dt(fb_con, ref dt_sorted_list, dg_sortiration,
                $"select * from select_all_users_from_group(\'{group_container.Text}\');");
            change_column_headers(dg_sortiration, dg_lu_column_headers);
        }

         void method_for_updation_dg_sort()
        {
            select_from_dt(fb_con, ref dt_sorted_list, dg_sortiration,
                $"select * from select_all_users_from_group(\'{group_container.Text}\');");
        }

        private void form_show_on_groups_Load(object sender, EventArgs e)
        {
            read_from_dt_to_comBox(ref group_container, dt_college_groups, 0);
        }

        private void delete_from_lu_Click(object sender, EventArgs e)
        {
            int index_selected_row;
            if (dg_sortiration.SelectedRows.Count == 0)
            {
                MessageBox.Show("Тут никого нет", "Вы никого не выбрали",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            index_selected_row = dg_sortiration.SelectedRows[0].Index;
            if ((int)dg_sortiration[5, index_selected_row].Value != 0)
            {
                MessageBox.Show("У пользователя еще есть книги на руках", 
                    "Пользователя не возможно удалить", 
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            insert_or_delete_in_dt(fb_con, ref dt_sorted_list,
                dg_sortiration,
                $"delete from library_users where (id_user =" +
                $" \'{dg_sortiration[0, index_selected_row].Value}\');",
                $"select * from select_all_users_from_group(\'{group_container.Text}\');");
            //select_from_dt(fb_con, ref dt_library_users, dg_library_users, load_all_from_dt_lu);
            updation?.Invoke();
        }

        private void extradition_of_books_Click(object sender, EventArgs e)
        {
            if (dg_sortiration.SelectedRows.Count == 0)
            {
                MessageBox.Show("Надо выбрать пользователя, которому надо выдать книгу",
                    "Не выбран пользователь", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            updation += method_for_updation_dg_sort;
            int index_of_select_row = dg_sortiration.SelectedRows[0].Index;
            extraditions_books_to_users form_for_extradition =
                new extraditions_books_to_users((int)dg_sortiration[0, index_of_select_row].Value,
                    dg_sortiration[1, index_of_select_row].Value.ToString(),
                dg_sortiration[2, index_of_select_row].Value.ToString(),
                dg_sortiration[4, index_of_select_row].Value.ToString());
            form_for_extradition.ShowDialog();
            form_for_extradition.Close();
            updation -= method_for_updation_dg_sort;
        }
    }
}
