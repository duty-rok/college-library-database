﻿using System;
using System.Windows.Forms;
using static additional_functions.additional_static_functions;
using System.Data;
using static library_database_application.Form1;
using FirebirdSql.Data.FirebirdClient;

namespace library_database_application
{
    public partial class add_in_lUdT : Form
    {
        string load_groups_from_dtcg = "select group_name from college_groups";
        
        //datatable for choosing group of college
        DataTable dt_groups_name;
        
        DataGridView dg_library_users;
        
        public add_in_lUdT()
        {
            InitializeComponent();
            not_change_form_size(this);
        }
        public add_in_lUdT(DataGridView dg_library_users) : this()
        {
            this.dg_library_users = dg_library_users;
        }

        private void add_in_lUdT_Load(object sender, EventArgs e)
        {
            /*adding in combobox items which to consists of exixts group names */
            select_from_dt(fb_con, ref dt_groups_name, load_groups_from_dtcg);
            read_from_dt_to_comBox(ref groups, dt_groups_name, 0);
        }

        //adding in library users
        private void button1_Click(object sender, EventArgs e)
        {
            if (surname.Text == "" || user_name.Text == "" || groups.Text == "") 
            {
                MessageBox.Show("Проверьте, все данные были указаны.\nПроверьте верность указанных данных", 
                    "Вы что-то не указали", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            insert_or_delete_in_dt(fb_con, ref dt_library_users,
                dg_library_users,
                $"insert into library_users values(\'{autoincrement_id(ref dt_library_users, 0)}\'," +
                $"\'{surname.Text}\',\'{user_name.Text}\',\'{patronymic.Text}\',\'{groups.Text}\',\'0\');",load_all_from_dt_lu);
            //select_from_dt(fb_con, ref dt_college_groups,dg_college_groups, load_all_from_dt_cg);
            updation?.Invoke();
            surname.Text = "";
            user_name.Text = "";
            patronymic.Text = "";
        }
    }
}
