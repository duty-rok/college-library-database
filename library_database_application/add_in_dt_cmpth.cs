﻿using System;
using System.Windows.Forms;
using static additional_functions.additional_static_functions;
using static library_database_application.Form1;

namespace library_database_application
{
    public partial class add_in_dt_cmpth : Form
    {
        DataGridView dataGridView;
        public add_in_dt_cmpth()
        {
            not_change_form_size(this);
            InitializeComponent();
        }

        public add_in_dt_cmpth(DataGridView dataGridView) : this()
        {
            this.dataGridView = dataGridView;
            date_publishing.Value = DateTime.Now;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (tilte.Text == "")
            {
                MessageBox.Show("Проверьте значение поля \"название\"", "Не указано название",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            insert_or_delete_in_dt(fb_con, ref dt_compositions, dataGridView,
                $"insert into compositions values(\'{autoincrement_id(ref dt_compositions, 0)}\'," +
                $"\'{tilte.Text}\',\'{date_publishing.Value.ToShortDateString()}\');", load_all_from_dt_cmpth);
            tilte.Text = "";
        }
    }
}
