﻿
namespace library_database_application
{
    partial class authors_compositions_relation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dg_available_authors = new System.Windows.Forms.DataGridView();
            this.tilte = new System.Windows.Forms.Label();
            this.link_author_with_compos = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.dg_exists_authors = new System.Windows.Forms.DataGridView();
            this.delete_authors = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.author_surname = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dg_available_authors)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dg_exists_authors)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dg_available_authors
            // 
            this.dg_available_authors.AllowUserToAddRows = false;
            this.dg_available_authors.AllowUserToDeleteRows = false;
            this.dg_available_authors.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dg_available_authors.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dg_available_authors.Location = new System.Drawing.Point(464, 178);
            this.dg_available_authors.MultiSelect = false;
            this.dg_available_authors.Name = "dg_available_authors";
            this.dg_available_authors.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dg_available_authors.Size = new System.Drawing.Size(296, 187);
            this.dg_available_authors.TabIndex = 0;
            // 
            // tilte
            // 
            this.tilte.Font = new System.Drawing.Font("DejaVu Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tilte.Location = new System.Drawing.Point(317, 9);
            this.tilte.Name = "tilte";
            this.tilte.Size = new System.Drawing.Size(443, 45);
            this.tilte.TabIndex = 1;
            this.tilte.Text = "label1";
            this.tilte.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // link_author_with_compos
            // 
            this.link_author_with_compos.Font = new System.Drawing.Font("DejaVu Sans Condensed", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.link_author_with_compos.Location = new System.Drawing.Point(317, 178);
            this.link_author_with_compos.Name = "link_author_with_compos";
            this.link_author_with_compos.Size = new System.Drawing.Size(141, 48);
            this.link_author_with_compos.TabIndex = 2;
            this.link_author_with_compos.Text = "Добавить автора к произведению";
            this.link_author_with_compos.UseVisualStyleBackColor = true;
            this.link_author_with_compos.Click += new System.EventHandler(this.link_author_with_compos_Click);
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("DejaVu Sans Condensed", 11.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(12, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(276, 45);
            this.label2.TabIndex = 3;
            this.label2.Text = "Произведение";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // dg_exists_authors
            // 
            this.dg_exists_authors.AllowUserToAddRows = false;
            this.dg_exists_authors.AllowUserToDeleteRows = false;
            this.dg_exists_authors.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dg_exists_authors.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dg_exists_authors.Location = new System.Drawing.Point(15, 178);
            this.dg_exists_authors.MultiSelect = false;
            this.dg_exists_authors.Name = "dg_exists_authors";
            this.dg_exists_authors.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dg_exists_authors.Size = new System.Drawing.Size(296, 187);
            this.dg_exists_authors.TabIndex = 4;
            // 
            // delete_authors
            // 
            this.delete_authors.Font = new System.Drawing.Font("DejaVu Sans Condensed", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.delete_authors.Location = new System.Drawing.Point(317, 316);
            this.delete_authors.Name = "delete_authors";
            this.delete_authors.Size = new System.Drawing.Size(141, 49);
            this.delete_authors.TabIndex = 5;
            this.delete_authors.Text = "Удалить авторство";
            this.delete_authors.UseVisualStyleBackColor = true;
            this.delete_authors.Click += new System.EventHandler(this.delete_authors_Click);
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("DejaVu Sans Condensed", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(461, 143);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(299, 23);
            this.label1.TabIndex = 6;
            this.label1.Text = "Авторы, которых можно добавить";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("DejaVu Sans Condensed", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(12, 143);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(299, 23);
            this.label3.TabIndex = 7;
            this.label3.Text = "Авторы, которые уже добавлены";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.author_surname);
            this.panel1.Location = new System.Drawing.Point(446, 57);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(314, 67);
            this.panel1.TabIndex = 10;
            // 
            // author_surname
            // 
            this.author_surname.Location = new System.Drawing.Point(123, 23);
            this.author_surname.Name = "author_surname";
            this.author_surname.Size = new System.Drawing.Size(180, 20);
            this.author_surname.TabIndex = 10;
            this.author_surname.TextChanged += new System.EventHandler(this.author_surname_TextChanged);
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(18, 17);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(99, 31);
            this.label4.TabIndex = 11;
            this.label4.Text = "Поиск автора по фамилии";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // authors_compositions_relation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(772, 373);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.delete_authors);
            this.Controls.Add(this.dg_exists_authors);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.link_author_with_compos);
            this.Controls.Add(this.tilte);
            this.Controls.Add(this.dg_available_authors);
            this.Name = "authors_compositions_relation";
            this.Text = "authors_compositions_realtion";
            this.Load += new System.EventHandler(this.authors_compositions_realtion_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dg_available_authors)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dg_exists_authors)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dg_available_authors;
        private System.Windows.Forms.Label tilte;
        private System.Windows.Forms.Button link_author_with_compos;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridView dg_exists_authors;
        private System.Windows.Forms.Button delete_authors;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox author_surname;
        private System.Windows.Forms.Label label4;
    }
}